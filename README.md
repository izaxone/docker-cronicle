# Docker Cronicle

Simple and lightweight Cronicle Docker solution, support x86, ARM64, ARMv7.

Say goodbye to troublesome configuration and installation, and start your Scheduled Task Center with One-Click.

### Default Account

default account and password:

- username: `admin`
- password: `admin`

## Docker

It is very simple to use, only **one command is needed**:

you can start a container that persists `data` and `log` files locally through Docker.

```bash
docker run \
        -v /etc/localtime:/etc/localtime:ro \
        -v /etc/timezone:/etc/timezone:ro \
        -v `pwd`/data/data:/opt/cronicle/data:rw \
        -v `pwd`/data/logs:/opt/cronicle/logs:rw \
        -v `pwd`/data/plugins:/opt/cronicle/plugins:rw \
        -p 3012:3012 \
        --hostname cronicle \
        --name cronicle \
        izaxone/cronicle
```

When the service is started, we can access the service in the browser: http://localhost:3012

## Docker Compose

Using `compose` to start our service also requires **only one command**:

```bash
docker-compose down && docker-compose up -d
```

When the service is started, we can access the service in the browser: http://localhost:3012

## With Traefik

You can refer to the configuration in `docker-compose.traefik.yml` file.
